// Public Key:
const pubKey = "BDGNzbZt9iJ0Sxqh-vTuNYPIDJGVvcRoCRMmYZP40SadjTegkY3wcXDpr4bB8rFtq-493XdRBHWaDX3tnFD1k1w"

// check if the service worker is active
if ("serviceWorker" in navigator) {
    send().catch(err => console.error(err));
}  

//register the service worker, register our push api, send the notification
async function send(){
    //register service worker
    const register = await navigator.serviceWorker.register('javascript/service-worker.js', {
        scope: '/javascript/'
    });

    //register push
    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        // use public key
        applicationServerKey: pubKey
    });
   
    //Send push notification
    await fetch("/notify/subscribe", {
        method: "POST",
        body: JSON.stringify(subscription),
        headers: {
            "content-type": "application/json"
        }
    });
}