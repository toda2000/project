// sends htttpRequest with given parameters
function sendHttpRequest(method, url, data, token) {
    return fetch(url, {
        method: method,
        body: JSON.stringify(data),
        headers: data ? { 
            'Content-Type': 'application/json',
            'x-access-header': token // this is to verify user against server
        } : {}

    }).then(response => {
        return response.json();
    });
}

// sets the form attributes for login request and submit
function sendLoginRequest() {
    sendHttpRequest('POST', '/authentication/login', {
        username: document.getElementById('username').value,
        password: document.getElementById('password').value
    }).then(response => {
        console.log(response);
        login(response.user.role);
    }).catch(err => {
        console.log(err);
    });
}

// sets the form attributes for login request and submit
function sendRegisterRequest() {
    sendHttpRequest('POST', '/authentication/register', {
        username: document.getElementById('username').value,
        password: document.getElementById('password').value,
        // fixed it so that it gets the role value (it default to basic otherwise)
        role: document.querySelector('input[name="role"]:checked').value
    }).then(response => {
        console.log(response);
    }).catch(err => {
        console.log(err);
    });
}

// sets the form attributes for login request and submit
// this one doesn't work
function sendUpdateRequest() {
    sendHttpRequest('PUT', '/user/:userId', {
        username: document.getElementById('username').value,
        password: document.getElementById('password').value,
        role: document.getElementById('role-selector').value
    })
    .then(response => {
        console.log(response);
    }).catch(err => {
        console.log(err);
    });
}

function getallHeaders() {
    var req = new XMLHttpRequest();
    req.open('GET', document.location, false);
    req.send(null);
    var headers = req.getAllResponseHeaders().toLowerCase();
    alert(headers);
}

// displays different parts depending on role (server should also check)
function login(role) {
    if (role === "basic") {
        document.getElementById("fo-lat").disabled = true
        document.getElementById("fo-lon").disabled = true
        document.getElementById("fo-pla").disabled = true
        document.getElementById("fo-tea").disabled = true
        document.getElementById("fo-sta").disabled = true
        document.getElementById("fo-dat").disabled = true
        document.getElementById("btn-add-user").style.display = "none"
        document.getElementById("btn-update-user").style.display = "none"
        document.getElementById("role-selector").style.display = "none"
        document.getElementById("options").style.display = "flex"
        document.getElementById("incident-form").style.display = "flex"
        document.getElementById("dispatcher").style.display = "flex"
        document.getElementById("team-label").style.display = "flex"
    } else if (role === "manager") {
        document.getElementById("fo-lat").disabled = false
        document.getElementById("fo-lon").disabled = false
        document.getElementById("fo-pla").disabled = false
        document.getElementById("fo-tea").disabled = false
        document.getElementById("fo-sta").disabled = false
        document.getElementById("fo-dat").disabled = false
        document.getElementById("btn-add-user").style.display = "none"
        document.getElementById("btn-update-user").style.display = "none"
        document.getElementById("options").style.display = "flex"
        document.getElementById("incident-form").style.display = "flex"
        document.getElementById("dispatcher").style.display = "flex"
    } else if (role === "admin") {
        document.getElementById("fo-lat").disabled = false
        document.getElementById("fo-lon").disabled = false
        document.getElementById("fo-pla").disabled = false
        document.getElementById("fo-tea").disabled = false
        document.getElementById("fo-sta").disabled = false
        document.getElementById("fo-dat").disabled = false
        document.getElementById("btn-add-user").style.display = "flex"
        document.getElementById("btn-update-user").style.display = "flex"
        document.getElementById("role-selector").style.display = "flex"
        document.getElementById("options").style.display = "flex"
        document.getElementById("incident-form").style.display = "flex"
        document.getElementById("dispatcher").style.display = "flex"
        document.getElementById("team-label").style.display = "flex"
    } else {
        document.getElementById("options").style.display = "none"
        document.getElementById("incident-form").style.display = "none"
        document.getElementById("dispatcher").style.display = "none"
        document.getElementById("team-label").style.display = "none"
    }
}

// not sure when to run this
login("none")

// Initialize and add the map
function initMap() {
    // default location
    const target = { lat: 59.6, lng: 18.4 };
    // center on default location
    new google.maps.Map(document.getElementById("map"), {
        zoom: 10,
        center: target,
        mapTypeControl: false
    });
}

// sets values on startup
window.addEventListener("load", function() {
    // appends all incidents
    function onResponse () {
        let response = JSON.parse(this.responseText)
        response.forEach(element => {
            let option = document.createElement("option")
            let text = document.createTextNode(element.place)
            option.appendChild(text)
            option.value = element._id
            document.getElementById("incident-select").appendChild(option)
        });
      }
      let request = new XMLHttpRequest();
      request.onload = onResponse;
      request.open("get", "incidents/incident", true);
      request.send();

    // appends all resources
    function onResponse2 () {
        let response2 = JSON.parse(this.responseText)
        response2.forEach(element2 => {
            let li = document.createElement("li")
            let text = document.createTextNode(element2.name + "-" + element2.team + "-" + element2.place)
            li.appendChild(text)
            document.getElementById("resources-list").appendChild(li)
        });
      }
      let request2 = new XMLHttpRequest();
      request2.onload = onResponse2;
      request2.open("get", "resource/resource", true);
      request2.send();

      // set date to today
      let date = new Date()
      document.getElementById("fo-dat").value = date.getFullYear() + "-" + date.getDate() + "-" + date.getDay()

},false)

// when selecting incident drop drop-down, change data to incident
document.getElementById("incident-select").addEventListener("change", function() {
    function onResponse () {
        let response = JSON.parse(this.responseText)
        console.log(response)
        document.getElementById("fo-lat").value = response.latitude
        document.getElementById("fo-lon").value = response.longitude
        document.getElementById("fo-pla").value = response.place
        document.getElementById("fo-tea").value = response.team
        document.getElementById("fo-sta").value = response.status
        document.getElementById("fo-dat").value = response.date
        let target = { lat: response.latitude, lng: response.longitude };
        new google.maps.Map(document.getElementById("map"), {
            zoom: 14,
            center: target,
            mapTypeControl: false
        });

      }
      
      let incidentId = document.getElementById("incident-select").value

      var request = new XMLHttpRequest();
      request.onload = onResponse;
      request.open("get", "incidents/incident/" + incidentId, true);
      request.send();
}, false)

// open dispatcher button
document.getElementById("btn-dispatch").addEventListener("click", function() {
    document.getElementById("dispatcher").style.visibility = "visible"
}, false)

// close dispatcher button
document.getElementById("btn-close-dispatcher").addEventListener("click", function() {
    document.getElementById("dispatcher").style.visibility = "hidden"
}, false)

// open user button
document.getElementById("btn-user").addEventListener("click", function() {
    document.getElementById("login").style.visibility = "visible"
}, false)

// close user button
document.getElementById("btn-close-login").addEventListener("click", function() {
    document.getElementById("login").style.visibility = "hidden"
}, false)

// send incident to server
document.getElementById("btn-send").addEventListener("click", function() {
    let url = "/incidents/incident";

    let request = new XMLHttpRequest();
    request.open("POST", url);

    request.setRequestHeader("Accept", "application/json");
    request.setRequestHeader("Content-Type", "application/json");

    request.onreadystatechange = function () {
    if (request.readyState === 4) {
        console.log(request.responseText);
    }};

    let content = {
        latitude : document.getElementById("fo-lat").value,
        longitude : document.getElementById("fo-lon").value,
        place : document.getElementById("fo-pla").value,
        team : document.getElementById("fo-tea").value,
        status : document.getElementById("fo-sta").value,
        date : document.getElementById("fo-dat").value
    };

    request.send(JSON.stringify(content));
}, false)

// login user
document.getElementById("btn-login").addEventListener("click", function() {
    sendLoginRequest();
}, false)

// add user
document.getElementById("btn-add-user").addEventListener("click", function() {
    sendRegisterRequest();
}, false)

// update user
document.getElementById("btn-update-user").addEventListener("click", function() {
    // sendUpdateRequest();
    alert("FUTURE FUNCTION")
}, false)

// save user team
const channel4Broadcast = new BroadcastChannel('team');
document.getElementById("team-select").value = 0
document.getElementById("team-select").value = localStorage.getItem('team')
document.getElementById("team-select").addEventListener("change", function() {
    console.log("team set to " + document.getElementById("team-select").value)
    channel4Broadcast.postMessage({team: document.getElementById("team-select").value});
}, false)

// add resource btn
document.getElementById("btn-add-resource").addEventListener("click", function() {
        let url = "/resource/resource";

        let request = new XMLHttpRequest();
        request.open("POST", url);
    
        request.setRequestHeader("Accept", "application/json");
        request.setRequestHeader("Content-Type", "application/json");
    
        request.onreadystatechange = function () {
        if (request.readyState === 4) {
            console.log(request.responseText);
        }};
    
        let content = {
            name : document.getElementById("rsname").value,
            place : document.getElementById("rsplace").value,
            team : document.getElementById("team-resource-select").value,
        };
    
        request.send(JSON.stringify(content));
})