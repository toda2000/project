const channel4Broadcast = new BroadcastChannel('team');
let team = 0;
channel4Broadcast.onmessage = (event) => {
    team = event.data.team;
}
self.addEventListener("push", e => {
    console.log("something was pushed")
    const notification = e.data.json();
    if (notification.team == team) {
        self.registration.showNotification(
            notification.title, // title of the notification
            {
                body: notification.text, // the text in the notification?
            }
        );
    }
});