const User = require('../model/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const roles = require('../roles');

// function to hash password with salt.
async function hashPassword(password) {
    return await bcrypt.hash(password, 10);
}

// function to validate password
async function validatePassword(password, hashedPassword) {
    return await bcrypt.compare(password, hashedPassword);
}

// register user
exports.registerUser = async (req, res, next) => {
    const {username, password, role} = req.body;
    if(!username || !password) return res.status(400).json({ message: 'Username and password are required.' });
    // check if username is taken
    const usernameExist = await User.findOne({username: username});
    if (usernameExist) return res.status(409).json({ message: 'Username already exists' });
    try {
        const hashedPassword = await hashPassword(password);

        // if no role is declared, default to basic
        const newUser = new User({
            username,
            password: hashedPassword,
            role: role || "basic"}
            );

        const accessToken = jwt.sign({userId: newUser._id}, process.env.TOKEN_SECRET, {
            expiresIn: "1d"
        });
        newUser.accessToken = accessToken;
        await newUser.save();
        res.json({
            "new user": newUser,
            accessToken
        })
    } catch (error) {
        next(error)
    }
}

// check user credentials and replace token
exports.login = async (req, res, next) => {
    const {username, password} = req.body;
    console.log(username)
    if(!username || !password) return res.status(400).json({ message: 'Username and password are required.' });
    try {

        const user = await User.findOne({ username });
        if (!user) return res.status(401).json({ error: 'Username does not exist' });

        const validPassword = await validatePassword(password, user.password);
        if (!validPassword) return res.status(401).json({ error: 'Password is incorrrect' });

        const accessToken = jwt.sign({ userId: user._id }, process.env.TOKEN_SECRET, {
            expiresIn: "1d"
        });

        res.header('x-access-token', accessToken)
        
        await User.findByIdAndUpdate(user._id, { accessToken })
        res.status(200).json({
            message: "log in successful!",
            user: { username: user.username, role: user.role, userId: user._id },
            accessToken
        })

    } catch (error) {
        console.log(error);
        next(error)
    }
}

// get all users from db
exports.getUsers = async (req, res, next) => {
    const users = await User.find();
    res.json({ users: users });
}

// get specific user from db
exports.getUser = async (req, res, next) => {
    try {
        const userId = req.params.userId;
        const user = await User.findById(userId);
        if (!user) return next(new Error('User does not exists'));
        res.json({ user: user });
    } catch (error) {
        next(error)
    }
}

// update user in db
exports.updateUser = async (req, res, next) => {
    try {
        const update = req.body;
        const username = req.body.username;
        const user = await User.findOne({username: username});
        const userId = user._id;

        const updatedUser = await User.findByIdAndUpdate(userId, update);
        // const user = await User.findById(userId);
        res.json({ user: user, message: 'user has been updated' });
    } catch (error) {
        res.json({ error: error });
    }
}

// delete user in db
exports.deleteUser = async (req, res, next) => {
    try {
        const userId = req.params.userId;
        await User.findByIdAndDelete(userId);
        res.json({ user: null, message: 'user has been deleted' });
    } catch (error) {
        next(error)
    }
}

// check to see if user has 
exports.grantAccess = function(action, resource) {
    return async (req, res, next) => {
        try {
            const permission = roles.can(req.user.role)[action](resource);
            if (!permission.granted) {
                return res.status(401).json({
                    error: "you don't have permission to perform this action"
                });
            }
            next()
        } catch (error) {
            next(error)
        }
    }
}
// check to see if user is logged in
exports.allowIfLoggedin = async (req, res, next) => {
    try {
     const user = res.locals.loggedInUser;
     if (!user)
      return res.status(401).json({
       error: "You need to be logged in to access this route"
      });
      req.user = user;
      next();
     } catch (error) {
      next(error);
     } 
}
