const AccessControl = require('accesscontrol');
const ac = new AccessControl();

ac.grant("basic")
.readAny("incident")
.updateAny("incident")
.readOwn("notification") // might be readAny ?
.readOwn("user")

ac.grant("manager")
.extend("basic")
.readAny("user")
.readAny("notification")
.createAny("notification")
.updateAny("notification")
.deleteAny("notification")
.createAny("incident")
.updateAny("incident")
.deleteAny("incident")

ac.grant("admin")
.extend("manager")
.createAny("user")
.updateAny("user")
.deleteAny("user")

/* // PERMISSIONS for different users
exports.grants = function () {
    ac.grant("basic")
    .readAny("incident")
    .updateAny("incident")
    .readOwn("notification") // might be readAny ?
    .readOwn("user")
    .updateAny("user")

    ac.grant("manager")
    .extend("basic")
    .readAny("user")
    .readAny("notification")
    .createAny("notification")
    .updateAny("notification")
    .deleteAny("notification")
    .createAny("incident")
    .updateAny("incident")
    .deleteAny("incident")

    ac.grant("admin")
    .extend("manager")
    .createAny("user")
    .updateAny("user")
    .deleteAny("user")

    return ac;
}; */
module.exports = ac;