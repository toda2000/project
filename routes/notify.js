const express = require('express');
const router = express.Router();
const webpush = require('web-push');
const mongoose = require('mongoose');

// get connection
const db = mongoose.connection;
// notify if connection fails
db.on('error', console.error.bind(console, 'connection error:'));

// set schemas and models
const Schema = mongoose.Schema;

// model detailing structure of incident data on server
const incidentSchema = new Schema({
  _id: Schema.Types.ObjectId,
  latitude: Number,
  longitude: Number,
  place: String,
  team_id: String,
  status: String,
  date: String
})

// model detailing structure of resource data on server
const resourceSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    team: String,
    place: String
  })

let Incident
try {
    Incident = mongoose.model('incidents')
} catch (error) {
    Incident = mongoose.model('incidents', incidentSchema);
}

let Resource
try {
    Resource = mongoose.model('resources')
} catch (error) {
    Resource = mongoose.model('resources', resourceSchema);
}

// Public Key:
const pubKey = "BDGNzbZt9iJ0Sxqh-vTuNYPIDJGVvcRoCRMmYZP40SadjTegkY3wcXDpr4bB8rFtq-493XdRBHWaDX3tnFD1k1w"

// Private Key:
const priKey = "AeMQJRhKo7GtIPhaySuPV6Ny9HCNh9ComA0pTwty7aI"

const subscriptions = []

webpush.setVapidDetails('mailto:maha2016@student.miun.se', pubKey,priKey);

// route accessed when subscribing
router.post('/subscribe', (req, res) => {

    // get subscription from request
    subscriptions.push(req.body);

    // notify that operation succeeded (it probably did)
    res.status(201).json();
})

db.once("open", () => {
    console.log("Setting emitter listeners");
    const emitter = Incident.watch()
    emitter.on('change', change => {
        console.log(change)
        if (change.fullDocument == null) {
            return
        }
        // create payload: specified the details of the push notification
        const payload = JSON.stringify({title: 'IMTS Alert!', text:'An incident has occurred!', team:change.fullDocument.team_id});
    
        // pass the object into sendNotification function and catch any error
        subscriptions.forEach(element => {
            webpush.sendNotification(element, payload).catch(err => console.error(err));
        })
    })
    const emitterR = Resource.watch()
    emitterR.on('change', change2 => {
        console.log(change2)
        if (change2.fullDocument == null) {
            return
        }
        // create payload: specified the details of the push notification
        const payload = JSON.stringify({title: 'DISPATCH RESOURCE ALERT!', text:'Dispatch ' + change2.fullDocument.name + ' to ' + change2.fullDocument.place, team:change2.fullDocument.team});
    
        // pass the object into sendNotification function and catch any error
        subscriptions.forEach(element => {
            webpush.sendNotification(element, payload).catch(err => console.error(err));
        })
    })
})



module.exports = router;