// components needed for managing database
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

// get connection
const db = mongoose.connection;
// notify if connection fails
db.on('error', console.error.bind(console, 'connection error:'));

// set schemas and models
const Schema = mongoose.Schema;

// model detailing structure of incident data on server
const incidentSchema = new Schema({
  _id: Schema.Types.ObjectId,
  latitude: Number,
  longitude: Number,
  place: String,
  team_id: String,
  status: String,
  date: String
})

let Incident
try {
    Incident = mongoose.model('incidents')
} catch (error) {
    Incident = mongoose.model('incidents', incidentSchema);
}

// get all incidents
router.get('/incident', function(req, res) {
  Incident.find(function(err, incidents) {
    if (err || incidents === undefined || incidents === null) {
      res.status(500).json({ message: "Unknown error!" })
      return console.error(err);
    } else {
      res.status(200).json(incidents)
    }
  });
})

// get one incident
router.get('/incident/:id', function(req, res) {
  Incident.findById(req.params.id, function(err, incident) {
    if(err || incident === undefined || incident === null) {
      res.status(404).json({ message: "ID does not correspond to any incident!" })
      if (err) {
        return console.error(err);
      }
    } else {
      res.status(200).json(incident)
    }
  });
})

// add incident to incident database
router.post('/incident', function(req, res) {
  let newIncident = new Incident({
    _id: new mongoose.Types.ObjectId(),
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    place: req.body.place,
    team_id: req.body.team,
    status: req.body.status,
    date: req.body.date
  })
  newIncident.save(function(err) {
    if (err) {
      // should not happen?
      res.status(409).json({ message: "Already added!"})
      return console.error(err);
    } else {
      res.status(201).json({ message: "Incident has been added!"})
    }
  });
})

// delete incident
router.delete('/incident/:id', function(req, res) {
  Incident.deleteOne({ _id: req.params.id }, function(err) {
    if (err) {
      res.status(404).json({ message: "Incident not deleted!" })
      return console.error(err);
    } else {
      res.status(200).json({ message: "Incident successfully deleted!" })
    }
  })
})

// update incident status and team
router.put('/incident/:id', function(req, res) {
  Incident.findById( req.params.id, function(err, incident) {
    if (incident !== null && incident!== undefined && !err) {
      incident.status = req.body.status;
      incident.team_id = req.body.team_id;
      incident.save(function(err) {
        if(err) return console.error(err);
    });
      res.status(200).json({ message: "Incident edited!" })
    } else {
      res.status(404).json({ message: "Incident does not exist!" })
      return console.error(err);
    }
  });
})

module.exports = router;