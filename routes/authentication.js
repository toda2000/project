// components needed for managing database
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const User = require('../model/User');
const userController = require('../controllers/userController');

dotenv.config();

// connect to DB
mongoose.connect(process.env.DB_CONNECT,
{useNewUrlParser: true},
() => console.log('Connected to db'));

// middleware to verify token
router.use(async (req, res, next) => {
	if (req.headers["x-access-token"]) {
		const accessToken = req.headers["x-access-token"];
		const { userId, exp } = jwt.verify(accessToken, process.env.TOKEN_SECRET);
		// check if token has expired
		if (exp < Date.now().valueOf() / 1000) {
			return res.status(401).json({
				error: "Your session has expired, please login again"
			});
		}
		res.locals.loggedInUser = await User.findById(userId);
		next();
	}	else {
		next();
	}
});


// TODO - this should only be allowed by admin
router.post('/register', userController.registerUser);

router.post('/login', userController.login);

router.get('/user/:userId', userController.allowIfLoggedin, userController.getUser);

router.get('/users', userController.allowIfLoggedin, userController.grantAccess('readAny', 'user'), userController.getUsers);

router.put('/user/:username', userController.allowIfLoggedin, userController.grantAccess('updateAny', 'user'), userController.updateUser);

router.delete('/user/:userId', userController.allowIfLoggedin, userController.grantAccess('deleteAny', 'user'), userController.deleteUser);

module.exports = router;