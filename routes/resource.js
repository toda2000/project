// components needed for managing database
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

// get connection
const db = mongoose.connection;
// notify if connection fails
db.on('error', console.error.bind(console, 'connection error:'));

// set schemas and models
const Schema = mongoose.Schema;

// model detailing structure of resource data on server
const resourceSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: String,
  team: String,
  place: String
})

let Resource
try {
    Resource = mongoose.model('resources')
} catch (error) {
    Resource = mongoose.model('resources', resourceSchema);
}

// get all resources
router.get('/resource', function(req, res) {
  Resource.find(function(err, resources) {
    if (err || resources === undefined || resources === null) {
      res.status(500).json({ message: "Unknown error!" })
      return console.error(err);
    } else {
      res.status(200).json(resources)
    }
  });
})

// add resource to resource database
router.post('/resource', function(req, res) {
  let newResource = new Resource({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    place: req.body.place,
    team: req.body.team,
  })
  newResource.save(function(err) {
    if (err) {
      // should not happen?
      res.status(409).json({ message: "Already added!"})
      return console.error(err);
    } else {
      res.status(201).json({ message: "Resource has been added!"})
    }
  });
})

module.exports = router;