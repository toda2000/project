// author: Marcus Haapasaari Lindgren

// required modules
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
// routes are used for specific functions, such as authentication
const authentication = require('./routes/authentication');
const incidents = require('./routes/incidents');
const notify = require('./routes/notify');
const resource = require('./routes/resource');


// set the express functionality
const app = express();
app.use(function(req, res, next) {
	// allow connections from all servers
	res.header("Access-Control-Allow-Origin", "*");
	// allowed client headers
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
	// allowed client methods
	res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
	next();
});

// set express to serve files in folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({extended: true}));
app.use(express.json())

// connect to DB
mongoose.connect(process.env.DB_CONNECT,
	{useNewUrlParser: true},
	() => console.log('Connected to db'));
	
// add additional routes
app.use("/authentication", authentication)
app.use("/incidents", incidents)
app.use("/notify", notify)
app.use("/resource", resource)

// adds some spacing to response JSON
app.set('json spaces', 2)

// port used is set for heroku compatibility
const port = process.env.PORT || 3000;


// start server
app.listen(port, function() {
  console.log(`Server is running on port ${port}`);
});