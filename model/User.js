const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    team: {
        type: String
    },
    role: {
        type: String,
        default: 'basic',
        enum: ["basic", "manager", "admin"]
    }
});

module.exports = mongoose.model('User', userSchema);